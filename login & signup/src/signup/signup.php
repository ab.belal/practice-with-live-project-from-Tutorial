<?php

class signup {

    public $id = '';
    public $uniqueId = '';
    public $verificationId = '';
    public $fName = '';
    public $lName = '';
    public $fullName = '';
    public $userName = '';
    public $validuser = '';
    public $email = '';
    public $validUserMail = '';
    public $loginEmail = '';
    public $password = '';
    public $repPassword = '';
    public $loginPassword = '';
    public $dbUser = 'root';
    public $dbPassword = '';
    public $dbConnect = '';
    public $error = '';
    public $dbAllData = '';
    public $dbSingleRowId = '';

    public function __construct() {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->dbConnect = new PDO("mysql:host=localhost; dbname=belalproject", $this->dbUser, $this->password) or die('Unable to connect with Database');
    }

    public function prepare($formAllData='') {
        if (array_key_exists('id', $formAllData)) {
            $this->id = $formAllData['id'];
        }

        if (array_key_exists('vid', $formAllData) && !empty($formAllData['vid'])) {
            $this->verificationId = $formAllData['vid'];
        }

        if (array_key_exists('fname', $formAllData) && !empty($formAllData['fname'])) {
            $this->fName = $formAllData['fname'];
        }

        if (array_key_exists('lname', $formAllData) && !empty($formAllData['lname'])) {
            $this->lName = $formAllData['lname'];
        }
        $fullName = array("$this->fName", "$this->lName");
        $this->fullName = implode(" ", $fullName);

        if (array_key_exists('userName', $formAllData) && !empty($formAllData['userName'])) {
            $this->userName = $formAllData['userName'];
        }

        if (array_key_exists('email', $formAllData) && !empty($formAllData['email'])) {
            $this->email = $formAllData['email'];
        }

        if (array_key_exists('password', $formAllData) && !empty($formAllData['password'])) {
            $this->password = $formAllData['password'];
        }

        if (array_key_exists('repPassword', $formAllData) && !empty($formAllData['repPassword'])) {
            $this->repPassword = $formAllData['repPassword'];
        }

        if (!empty($formAllData['singleRowId'])) {
            $this->dbSingleRowId = $formAllData['singleRowId'];
        }


        //for login form data
        if (!empty($formAllData['loginEmail'])) {
            $this->loginEmail = $formAllData['loginEmail'];
        } else {
            $_SESSION['errEmail'] = "Email Required";
//            header('location:login.php');
        }
        if (!empty($formAllData['loginPassword'])) {
            $this->loginPassword = $formAllData['loginPassword'];
        } else {
            $_SESSION['errPassword'] = "Password Required";
//            header('location:login.php');
        }

        $_SESSION['formsData'] = $formAllData;
        return $this;
    }

    //end prepare method
    //
    //
    //
    //
     public function errMsg($errMsg = '') {
        if (isset($_SESSION["$errMsg"]) && !empty($_SESSION["$errMsg"])) {
            echo $_SESSION["$errMsg"];
            unset($_SESSION["$errMsg"]);
        }
    }

    //end errMsg method
    //
    //
    //
    //
    //
    //
        public function signupValidation() {
        //for user name validation
        if (!empty($this->userName)) {
            if (strlen($this->userName) >= 5 && strlen($this->userName) <= 12) {

                $userName = "'$this->userName'";
                $selectQuery = "SELECT * FROM `signup` WHERE `userName` = " . $userName;
                $statement = $this->dbConnect->prepare($selectQuery);
                $statement->execute();
                $this->validuser = $statement->fetch(PDO::FETCH_ASSOC);

                if (!empty($this->validuser)) {
                    $_SESSION['errName'] = "Username already Exist try to another one";
                    $this->error = true;
                    header('location:create.php');
                }
            } else {
                $_SESSION['errName'] = "User Name must be <span style='font-weight:bold;'>5 to 12</span> Charecter";
                $this->error = true;
                header('location:create.php');
            }
        } else {
            $_SESSION['errName'] = "User Name required";
            $this->error = true;
            header('location:create.php');
        }


        //for email validation
        if (!empty($this->email)) {
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) {

                $email = "'$this->email'";
                $selectQuery = "SELECT * FROM `signup` WHERE `email` = " . $email;
                $statement = $this->dbConnect->prepare($selectQuery);
                $statement->execute();
                $this->validUserMail = $statement->fetch(PDO::FETCH_ASSOC);

                if (!empty($this->validUserMail)) {
                    $_SESSION['errEmail'] = "Email already Exist try to another one";
                    $this->error = true;
                    header('location:create.php');
                }
            } else {
                $_SESSION['errEmail'] = "Email address is not valid";
                $this->error = true;
                header('location:create.php');
            }
        } else {
            $_SESSION['errEmail'] = "Email required";
            $this->error = true;
            header('location:create.php');
        }


        //for password and repeat password validation
        if (!empty($this->password)) {
            if (strlen($this->password) >= 5 && strlen($this->password) <= 12) {
                if (!empty($this->repPassword)) {
                    if ($this->password !== $this->repPassword) {
                        $_SESSION['errRepPassword'] = "Password doses't match try again";
                        $this->error = true;
                        header('location:create.php');
                    }
                } else {
                    $_SESSION['errRepPassword'] = "Repeat Password";
                    $this->error = true;
                    header('location:create.php');
                }
            } else {
                $_SESSION['errPassword'] = "Password must be <span style='font-weight:bold;'>5 to 12</span> Charecter";
                $this->error = true;
                header('location:create.php');
            }
        } else {
            $_SESSION['errPassword'] = "Password required";
            $this->error = true;
            header('location:create.php');
        }
    }

    //end signupValidation method
    //
    //
    //
    //
    
    public function store() {
        if ($this->error == FALSE) {
            try {
                $verificationId = uniqid();

                $insertQuery = "INSERT INTO `signup` (`id`, `uniqueId`, `verificationId`, `userName`, `fullName`, `email`, `password`, `isActive`, `isAdmin`, `created`) 
                    VALUES (:id, :uniqueId, :verificationId, :userName, :fullName, :email, :password, :isActive, :isAdmin, :created)";
                $statment = $this->dbConnect->prepare($insertQuery);
                $statment->execute(array(
                    ':id' => null,
                    ':uniqueId' => uniqid(),
                    ':verificationId' => $verificationId,
                    ':userName' => $this->userName,
                    ':fullName' => $this->fullName,
                    ':email' => $this->email,
                    ':password' => md5($this->password),
                    'isActive' => 0,
                    'isAdmin' => 0,
                    'created' => date("Y-m-d h:i:s"),
                ));
                $_SESSION['successMSg'] = "Successfully signup. Check your email for Verification";
                header("location:active.php?vid=$verificationId");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header('location:create.php');
        }
    }

//end store methode
//
//
//
//
//
    public function dbAllData() {
        try {
            $selectQuery = "SELECT * FROM `signup` ORDER BY `id` DESC";
            $stmt = $this->dbConnect->prepare($selectQuery);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->dbAllData[] = $row;
            }
            return $this->dbAllData;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

//end dbAllData methode
//
//
//
//
//

    public function dbSingleRowShow() {
        $singleRowUniqId = "'$this->dbSingleRowId'";

        $selectQuery = "SELECT * FROM `signup` WHERE `uniqueId`=" . $singleRowUniqId;
        $stmt = $this->dbConnect->prepare($selectQuery);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

//end dbAllData methode
//
//
//
//
//
    public function verification() {
        $verificationId = "'$this->verificationId'";
        $selectQuery = "SELECT * FROM `signup` WHERE `verificationId`= " . $verificationId;
        $statement = $this->dbConnect->prepare($selectQuery);
        $statement->execute();
        $user = $statement->fetch(PDO::FETCH_ASSOC);

        if ($user['isActive'] == 1) {
            $_SESSION['verifyedMsg'] = "Email Already Verified. Please Login";
            header('location:login.php');
        } else {
            $updateQuery = "UPDATE `signup` SET isActive = 1 WHERE `verificationId` =" . $verificationId;
            $stmt = $this->dbConnect->prepare($updateQuery);
            $stmt->execute();

            $_SESSION['verifyedMsg'] = "You are Now verified. Login Now";
            header('location:login.php');
        }
    }

//end verification methode
//
//
//
//
//
    public function login() {
        $logingEmail = "'$this->loginEmail'";
        $loginPassword = "'" . md5($this->loginPassword) . "'";

        $selectQuery = "SELECT * FROM `signup` WHERE `email`= $logingEmail AND `password`=$loginPassword";
        $loginStmt = $this->dbConnect->prepare($selectQuery);
        $loginStmt->execute();
        $userRow = $loginStmt->fetch(PDO::FETCH_ASSOC);

        if (!empty($userRow['email']) && !empty($userRow['password'])) {
            if ($userRow['isActive'] == 1) {
                $_SESSION['loginedUser'] = $userRow;
                header('location:dashboard.php');
            } else {
                $_SESSION['loginErr'] = "Your account not verified yet. Check your email and verify";
                header('location:login.php');
            }
        } else {
            $_SESSION['loginErr'] = "Invalid User Name or Password";
            header('location:login.php');
        }
    }

//end login methode
//
//
//
//
//
    public function logout() {
        try {
            unset($_SESSION['loginedUser']);
            $_SESSION['verifyedMsg'] = "Successfully logout";
            header("location:login.php");
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

//end logout methode
//
//
//
//
//
}

//end class
?>
